FROM node:8.9.0

# The base node image sets a very verbose log level.
ENV NPM_CONFIG_LOGLEVEL warn

RUN mkdir -p /opt/app

# Defaults to production
ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

# Defaults to port 3000
ARG PORT=3000
ENV PORT $PORT
EXPOSE $PORT

WORKDIR /opt/app

COPY ./package.json /opt/app
COPY ./package-lock.json /opt/app

RUN npm install

# FIXME: currently commented out because admin user cannot write the build folder
# This is to prevent running the container as root.
# RUN adduser -D -u 9999 admin
# USER admin

COPY . /opt/app

CMD ["echo", "We'll need to figure this command out later"]

