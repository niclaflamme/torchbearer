const path = require('path');
const paths = require('./paths');

module.exports = {
  components: path.resolve(paths.appSrc, 'components'),
  theme: path.resolve(paths.appSrc, 'theme'),
};
