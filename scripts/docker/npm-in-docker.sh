#!/bin/bash

CONTAINER_NAME='styleguide_devserver'
COMMAND=$1;
shift;
ARGUMENTS=$@

if [ ! $(docker ps -q -f name=$CONTAINER_NAME) ]; then
   echo "$CONTAINER_NAME is not currenlty running."
   echo "Starting ${CONTAINER_NAME}..."
   npm run dev
fi


docker container exec $CONTAINER_NAME bash -c "npm run -s $COMMAND -- $ARGUMENTS"
