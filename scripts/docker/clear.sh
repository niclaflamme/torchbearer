#!/bin/bash

usage() {
  echo
  echo "A cleanup script to remove cached docker files from your machine"
  echo
  echo "npm run docker:clear -- [OPTIONS]"]
  echo
  echo "Options:"
  echo
  echo "  -c  --containers        Cleanup containers"
  echo "  -i  --images            Cleanup images"
  echo "  -v  --volumes           Cleanup volumes - warning, this could destroy persistent data"
  echo
}

remove_containers() {
  echo
  echo "Removing containers..."
  echo
  docker rm $(docker ps -a -f status=exited -f status=created -q)
  echo
  echo "Containers removed."
}

remove_images() {
  echo
  echo "Removing images..."
  echo
  docker rmi $(docker images -a -q)
  echo
  echo "Images removed."
}

remove_volumes() {
  echo
  echo "Removing volues..."
  echo
  docker volume rm $(docker volume ls -f dangling=true -q)
  echo
  echo "Volumes removed."
}

confirm_remove_volumes() {
  while true; do
    read -p  "Volume data is non recoverable, are you sure you wish to continue? [Yes|No] " yn
    case $yn in
        [Yy]* ) break;;
        [Nn]* ) exit;;
        * ) echo "Your options are either 'Yes' or 'No'";;
    esac
  done
}


if [ $# -eq 0 ]
  then
    usage
    echo "You haven't provided any arguments"
    exit 1
fi

while [ "$1" != "" ]; do
    case $1 in
        -c | --containers )     shift
                                remove_containers
                                ;;
        -i | --images )         shift
                                remove_images
                                ;;
        -v | --volumes )        shift
                                confirm_remove_volumes
                                remove_volumes
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

echo
echo
echo
echo "Done. Enjoy the free space."
exit



